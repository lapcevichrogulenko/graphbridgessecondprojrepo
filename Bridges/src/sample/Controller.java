package sample;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.util.ArrayList;

public class Controller {


    public ArrayList<Vertex> buttons = new ArrayList<Vertex>();


    @FXML
    private Pane myPane;
    public void createButton(MouseEvent event) {

        if(event.getClickCount()==2) {
            Circle but = new Circle(10);
            Vertex vert = new Vertex(but);
            vert.circle.setCenterX(event.getX());
            vert.circle.setCenterY(event.getY());
            buttons.add(vert);
            myPane.getChildren().add(vert.circle);
        }
        else
        {
            if(event.getClickCount()==1)
            {

                for (Vertex one: buttons
                        ) {
                    if(one.isChecked)
                    {
                        one.circle.setFill(Color.BLACK);
                        one.isChecked=false;
                    }
                    else {
                        boolean inX = event.getX() < one.circle.getCenterX() + 10 && event.getX() > one.circle.getCenterX() - 10;
                        boolean inY = event.getY() < one.circle.getCenterY() + 10 && event.getY() > one.circle.getCenterY() - 10;
                        if (inX && inY) {
                            one.circle.setFill(Color.RED);
                            one.isChecked = true;
                        }
                    }
                }
            }
        }
    }
}