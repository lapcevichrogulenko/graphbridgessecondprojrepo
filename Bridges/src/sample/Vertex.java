package sample;

import javafx.scene.shape.Circle;

/**
 * Created by Кирилл on 26.06.2016.
 */
public class Vertex {
    public Circle circle;
    public boolean isChecked;
    Vertex (Circle circle)
    {
        this.circle=circle;
        isChecked=false;
    }
}
